package httpclient

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_http/httputils"
	"github.com/valyala/fasthttp"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

const methodGet = "GET"
const methodPost = "POST"
const methodPut = "PUT"
const methodDelete = "DELETE"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type HttpClient struct {
	client *fasthttp.Client
	header map[string]string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewHttpClient() *HttpClient {
	instance := new(HttpClient)
	instance.header = make(map[string]string)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *HttpClient) AddHeader(key, value string) {
	if nil != instance && nil != instance.header {
		instance.header[key] = value
	}
}

func (instance *HttpClient) RemoveHeader(key string) {
	if nil != instance && nil != instance.header {
		delete(instance.header, key)
	}
}

func (instance *HttpClient) Get(url string) (*httputils.ResponseData, error) {
	// return instance.GetTimeout(url, time.Second*15)
	return instance.do(methodGet, url, nil, time.Second*15)
}

func (instance *HttpClient) GetTimeout(url string, timeout time.Duration) (*httputils.ResponseData, error) {
	// return instance.get(url, timeout)
	return instance.do(methodGet, url, nil, timeout)
}

func (instance *HttpClient) Post(url string, body interface{}) (*httputils.ResponseData, error) {
	// return instance.GetTimeout(url, time.Second*15)
	return instance.do(methodPost, url, body, time.Second*15)
}

func (instance *HttpClient) PostTimeout(url string, body interface{}, timeout time.Duration) (*httputils.ResponseData, error) {
	// return instance.get(url, timeout)
	return instance.do(methodPost, url, body, timeout)
}

func (instance *HttpClient) Put(url string, body interface{}) (*httputils.ResponseData, error) {
	// return instance.GetTimeout(url, time.Second*15)
	return instance.do(methodPut, url, body, time.Second*15)
}

func (instance *HttpClient) PutTimeout(url string, body interface{}, timeout time.Duration) (*httputils.ResponseData, error) {
	// return instance.get(url, timeout)
	return instance.do(methodPut, url, body, timeout)
}

func (instance *HttpClient) Delete(url string, body interface{}) (*httputils.ResponseData, error) {
	// return instance.GetTimeout(url, time.Second*15)
	return instance.do(methodDelete, url, body, time.Second*15)
}

func (instance *HttpClient) DeleteTimeout(url string, body interface{}, timeout time.Duration) (*httputils.ResponseData, error) {
	// return instance.get(url, timeout)
	return instance.do(methodDelete, url, body, timeout)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *HttpClient) init() *fasthttp.Client {
	if nil == instance.client {
		instance.client = new(fasthttp.Client)
		instance.client.Name = "lygo_http_client"
	}
	return instance.client
}

func (instance *HttpClient) do(method string, uri string, reqBody interface{}, timeout time.Duration) (*httputils.ResponseData, error) {
	var err error

	// request
	req := fasthttp.AcquireRequest()
	req.Header.SetMethod(method)
	req.SetRequestURI(uri)

	if nil != instance.header {
		// fmt.Println(req.Header.String())
		for k, v := range instance.header {
			req.Header.Set(k, v)
		}
	}

	if nil != reqBody {
		if v, b := reqBody.(string); b {
			req.SetBodyString(v)
		} else if v, b := reqBody.([]byte); b {
			req.SetBody(v)
		} else if v, b := reqBody.([]uint8); b {
			req.SetBody(v)
		} else if v, b := reqBody.(map[string]interface{}); b {
			req.SetBodyString(lygo_json.Stringify(v))
		}
	}

	// response
	res := fasthttp.AcquireResponse()

	defer func() {
		fasthttp.ReleaseRequest(req)
		fasthttp.ReleaseResponse(res)
	}()

	client := instance.init()
	err = client.DoTimeout(req, res, timeout)

	return httputils.NewResponseData(res), err
}

func (instance *HttpClient) get(uri string, timeout time.Duration) (statusCode int, body []byte, err error) {
	client := instance.init()
	return client.GetTimeout(nil, uri, timeout)
}
