package lygo_http_server

import (
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_http/lygo_http_server/lygo_http_server_config"
	"bitbucket.org/lygo/lygo_ext_http/lygo_http_server/lygo_http_server_types"
	"crypto/tls"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type HttpServerService struct {
	Key string

	//-- private --//
	enabled bool
	app     *fiber.App
	// config
	config           *lygo_http_server_config.HttpServerConfig
	configHost       *lygo_http_server_config.HttpServerConfigHost
	configRoute      *lygo_http_server_config.HttpServerConfigRoute
	configMiddleware []*lygo_http_server_config.HttpServerConfigRouteItem
	configWebsocket  []*HttpServerConfigRouteWebsocket
	// callbacks
	callbackError        lygo_http_server_types.CallbackError
	callbackLimitReached lygo_http_server_types.CallbackLimitReached
}

//----------------------------------------------------------------------------------------------------------------------
//	HttpServerEndpoint
//----------------------------------------------------------------------------------------------------------------------

func NewServerService(key string,
	config *lygo_http_server_config.HttpServerConfig,
	host *lygo_http_server_config.HttpServerConfigHost,
	route *lygo_http_server_config.HttpServerConfigRoute,
	middleware []*lygo_http_server_config.HttpServerConfigRouteItem,
	websocket []*HttpServerConfigRouteWebsocket,
	callbackError lygo_http_server_types.CallbackError,
	callbackLimitReached lygo_http_server_types.CallbackLimitReached) *HttpServerService {

	instance := new(HttpServerService)
	instance.Key = key

	// fiber settings
	instance.config = config
	instance.configHost = host
	instance.configRoute = route
	instance.configMiddleware = middleware
	instance.configWebsocket = websocket

	instance.callbackError = callbackError
	instance.callbackLimitReached = callbackLimitReached

	instance.enabled = config.IsEnabled()

	return instance
}

func (instance *HttpServerService) IsEnabled() bool {
	if nil != instance {
		return instance.enabled
	}
	return false
}

func (instance *HttpServerService) Shutdown() error {
	if nil != instance && instance.enabled && nil != instance.app {
		return instance.app.Shutdown()
	}
	return nil
}

func (instance *HttpServerService) Open() {
	if nil != instance && instance.enabled {
		instance.init()
		if nil != instance.app {
			go instance.listen()
		}
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *HttpServerService) init() {
	config := instance.config
	host := instance.configHost
	if instance.enabled {

		// creates app configuration
		appConfig := fiber.Config{
			ServerHeader:  config.ServerHeader,
			Prefork:       config.Prefork,
			CaseSensitive: config.CaseSensitive,
			StrictRouting: config.StrictRouting,
			Immutable:     config.Immutable,
		}
		if config.BodyLimit > 0 {
			appConfig.BodyLimit = config.BodyLimit
		}
		if config.ReadTimeout > 0 {
			appConfig.ReadTimeout = config.ReadTimeout * time.Millisecond
		}
		if config.WriteTimeout > 0 {
			appConfig.WriteTimeout = config.WriteTimeout * time.Millisecond
		}
		if config.IdleTimeout > 0 {
			appConfig.IdleTimeout = config.IdleTimeout * time.Millisecond
		}
		instance.app = fiber.New(appConfig)

		recoverConfig := recover.Config{
			//Handler: instance.onServerError,
			Next: nil,
		}
		instance.app.Use(recover.New(recoverConfig))

		if config.EnableRequestId {
			instance.app.Use(requestid.New())
		}

		// CORS
		initCORS(instance.app, instance.config.CORS)

		// compression
		initCompression(instance.app, instance.config.Compression)

		// limiter
		initLimiter(instance.app, instance.config.Limiter, instance.onLimitReached)

		// Middleware
		if len(instance.configMiddleware) > 0 {
			initMiddleware(instance.app, instance.configMiddleware)
		}

		// Route
		if nil != instance.configRoute {
			initRoute(instance.app, instance.configRoute, nil)
		}

		// websocket
		socket := NewHttpWebsocket(instance.app, host, instance.configWebsocket)
		socket.Init()

		// Static
		if len(config.Static) > 0 {
			for _, static := range config.Static {
				if static.Enabled && len(static.Prefix) > 0 && len(static.Root) > 0 {
					instance.app.Static(static.Prefix, static.Root, fiber.Static{
						Compress:  static.Compress,
						ByteRange: static.ByteRange,
						Browse:    static.Browse,
						Index:     static.Index,
					})
				}
			}
		}
	}
}

func (instance *HttpServerService) listen() {
	app := instance.app
	host := instance.configHost
	var tlsConfig *tls.Config
	if host.TLS && len(host.SslKey) > 0 && len(host.SslCert) > 0 {
		cer, err := tls.LoadX509KeyPair(host.SslCert, host.SslKey)
		if err != nil {
			instance.doError("Error loading Certificates", err, nil)
		}
		tlsConfig = &tls.Config{Certificates: []tls.Certificate{cer}}
	}

	if nil == tlsConfig {
		if err := app.Listen(host.Address); err != nil {
			instance.doError(lygo_strings.Format("Error Opening channel: '%s'", host.Address), err, nil)
		}
	} else {
		ln, err := tls.Listen("tcp", host.Address, tlsConfig)
		if err != nil {
			instance.doError(lygo_strings.Format("Error Opening TLS channel: '%s'", host.Address), err, nil)
		} else {
			// Start server with https/ssl enabled on http://localhost:443
			if err := app.Listener(ln); err != nil {
				instance.doError(lygo_strings.Format("Error Opening TLS channel: '%s'", host.Address), err, nil)
			}
		}
	}
}

func (instance *HttpServerService) onServerError(c *fiber.Ctx, err error) {
	_ = c.SendString(err.Error())
	_ = c.SendStatus(500)
}

func (instance *HttpServerService) onLimitReached(c *fiber.Ctx) error {
	// request limit reached
	if nil != instance.callbackLimitReached {
		return instance.callbackLimitReached(c)
	}
	return c.SendStatus(fiber.StatusTooManyRequests)
}

func (instance *HttpServerService) doError(message string, err error, ctx *fiber.Ctx) {
	go func() {
		if nil != instance.callbackError {
			instance.callbackError(&lygo_http_server_types.HttpServerError{
				Sender:  instance,
				Message: message,
				Context: ctx,
				Error:   err,
			})
		}
	}()
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func initCORS(app *fiber.App, corsCfg *lygo_http_server_config.HttpServerConfigCORS) {
	if nil != corsCfg && corsCfg.Enabled {
		config := cors.Config{}
		if corsCfg.MaxAge > 0 {
			config.MaxAge = corsCfg.MaxAge
		}
		if corsCfg.AllowCredentials {
			config.AllowCredentials = true
		}
		if len(corsCfg.AllowMethods) > 0 {
			config.AllowMethods = strings.Join(corsCfg.AllowMethods, ",")
		}
		if len(corsCfg.AllowOrigins) > 0 {
			config.AllowOrigins = strings.Join(corsCfg.AllowOrigins, ",")
		}
		if len(corsCfg.ExposeHeaders) > 0 {
			config.ExposeHeaders = strings.Join(corsCfg.ExposeHeaders, ",")
		}
		app.Use(cors.New(config))
	}
}

func initCompression(app *fiber.App, cfg *lygo_http_server_config.HttpServerConfigCompression) {
	if nil != cfg && cfg.Enabled {
		config := compress.Config{}
		config.Level = compress.Level(cfg.Level)

		app.Use(compress.New(config))
	}
}

func initLimiter(app *fiber.App, cfg *lygo_http_server_config.HttpServerConfigLimiter, handler func(ctx *fiber.Ctx) error) {
	if nil != cfg && cfg.Enabled {
		config := limiter.Config{}
		if cfg.Max > 0 {
			config.Max = cfg.Max
		}
		if cfg.Duration > 0 {
			config.Duration = cfg.Duration
		}

		config.LimitReached = handler

		app.Use(limiter.New(config))
	}
}

func initMiddleware(app *fiber.App, items []*lygo_http_server_config.HttpServerConfigRouteItem) {
	for _, item := range items {
		path := item.Path
		if len(path) == 0 {
			app.Use(item.Handlers[0])
		} else {
			app.Use(path, item.Handlers[0])
		}
	}
}

func initRoute(app *fiber.App, route *lygo_http_server_config.HttpServerConfigRoute, parent fiber.Router) {
	for k, i := range route.Data {
		initRouteItem(app, k, i, parent)
	}
}

func initGroup(app *fiber.App, group *lygo_http_server_config.HttpServerConfigGroup, parent fiber.Router) {
	var g fiber.Router
	if nil == parent {
		g = app.Group(group.Path, group.Handlers...)
	} else {
		g = parent.Group(group.Path, group.Handlers...)
	}
	if nil != g && len(group.Children) > 0 {
		for _, c := range group.Children {
			if cc, b := c.(*lygo_http_server_config.HttpServerConfigGroup); b {
				// children is a group
				initGroup(app, cc, g)
			} else if cc, b := c.(*lygo_http_server_config.HttpServerConfigRoute); b {
				// children is route
				initRoute(app, cc, g)
			}
		}
	}
}

func initRouteItem(app *fiber.App, key string, item interface{}, parent fiber.Router) {
	method := lygo_strings.SplitAndGetAt(key, "_", 0)
	switch method {
	case "GROUP":
		v := item.(*lygo_http_server_config.HttpServerConfigGroup)
		initGroup(app, v, parent)
	case "ALL":
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.All(v.Path, v.Handlers...)
		} else {
			parent.All(v.Path, v.Handlers...)
		}
	case fiber.MethodGet:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Get(v.Path, v.Handlers...)
		} else {
			parent.Get(v.Path, v.Handlers...)
		}
	case fiber.MethodPost:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Post(v.Path, v.Handlers...)
		} else {
			parent.Post(v.Path, v.Handlers...)
		}
	case fiber.MethodOptions:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Options(v.Path, v.Handlers...)
		} else {
			parent.Options(v.Path, v.Handlers...)
		}
	case fiber.MethodPut:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Put(v.Path, v.Handlers...)
		} else {
			parent.Put(v.Path, v.Handlers...)
		}
	case fiber.MethodHead:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Head(v.Path, v.Handlers...)
		} else {
			parent.Head(v.Path, v.Handlers...)
		}
	case fiber.MethodPatch:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Patch(v.Path, v.Handlers...)
		} else {
			parent.Patch(v.Path, v.Handlers...)
		}
	case fiber.MethodDelete:
		v := item.(*lygo_http_server_config.HttpServerConfigRouteItem)
		if nil == parent {
			app.Delete(v.Path, v.Handlers...)
		} else {
			parent.Delete(v.Path, v.Handlers...)
		}
	}
}
