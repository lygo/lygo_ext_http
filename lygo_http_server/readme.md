#HTTP Server

Server uses [GoFiber](https://github.com/gofiber/fiber)

Further info on [Wiki](https://fiber.wiki/) 
 

```
go get -u github.com/gofiber/fiber/v2
go get -u github.com/gofiber/fiber/v2/middleware/recover
go get -u github.com/gofiber/fiber/v2/middleware/cors
go get -u github.com/gofiber/fiber/v2/middleware/compress
go get -u github.com/gofiber/fiber/v2/middleware/limiter
go get -u github.com/gofiber/fiber/v2/middleware/requestid

go get -u github.com/gofiber/websocket/v2
```