package test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_http/lygo_http_server"
	"bitbucket.org/lygo/lygo_ext_http/lygo_http_server/lygo_http_server_config"
	"bitbucket.org/lygo/lygo_ext_http/lygo_http_server/lygo_http_server_types"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"os"
	"testing"
)

func TestBasic(t *testing.T) {

	// load configuration
	config := config()
	if nil == config {
		t.Errorf("Configuration is not valid")
	}

	server := lygo_http_server.NewHttpServer(config)
	server.CallbackError = onError

	server.Route.Get("*", func(ctx *fiber.Ctx) error {
		_, err := ctx.Write([]byte("ROOT\n"))
		if nil != err {
			return err
		}
		return ctx.Next()
	})

	server.Route.Get("/get", func(ctx *fiber.Ctx) error {
		_, err := ctx.Write([]byte(fmt.Sprintf("Hi, I'm worker #%v", os.Getpid())))
		// ctx.SendBytes([]byte("THIS IS GET API"))
		return err
	})

	g := server.Route.Group("/api", func(ctx *fiber.Ctx) error {
		err := ctx.Send([]byte("THIS IS GROUP API\n"))
		if nil != err {
			return err
		}
		return ctx.Next()
	})
	g.Get("/v1", func(ctx *fiber.Ctx) error {
		_, err := ctx.WriteString("/v1\n")
		if nil != err {
			return err
		}
		_, err = ctx.Write([]byte("THIS IS v1"))
		return err
	})

	server.Middleware("/", func(ctx *fiber.Ctx) error {
		fmt.Println("First middleware")
		ctx.Append("middleware", "First middleware")
		return ctx.Next()
	})

	server.Middleware(func(ctx *fiber.Ctx) error {
		fmt.Println("Second middleware")
		ctx.Append("middleware", "Second middleware")
		return ctx.Next()
	})

	server.Middleware("/api", func(ctx *fiber.Ctx) error {
		fmt.Println("API middleware")
		_, err := ctx.WriteString("API middleware")
		if nil!=err{
			return err
		}
		ctx.Append("middleware", "API middleware")
		return ctx.Next()
	})

	server.Websocket("/websocket", onSocket)

	server.Middleware("/yoda", func(ctx *fiber.Ctx) error {
		// NOT FOUND
		b, _ := lygo_io.ReadBytesFromFile("./www/yoda.jpeg")
		return ctx.Send(b)
	})

	// start server
	err := server.Start()
	if nil != err {
		t.Error(err)
	}

	// Wait forever.
	server.Join()
}

func TestApi(t *testing.T) {

	// load configuration
	config := config()
	if nil == config {
		t.Errorf("Configuration is not valid")
	}

	server := lygo_http_server.NewHttpServer(config)
	server.CallbackError = onError

	server.Api("/api/*", func(ctx *fiber.Ctx) error {
		context := lygo_http_server.WrapContext(ctx)
		if nil != context {
			query := context.GetParams()
			if nil != query {
				fmt.Println(query)
			}
		}
		return nil
	})

	// start server
	err := server.Start()
	if nil != err {
		t.Error(err)
	}

	// Wait forever.
	server.Join()
}

func TestUpload(t *testing.T) {

	// load configuration
	config := config()
	if nil == config {
		t.Errorf("Configuration is not valid")
	}

	server := lygo_http_server.NewHttpServer(config)
	server.CallbackError = onError

	server.Api("/upload/*", func(ctx *fiber.Ctx) error {
		// Parse the multipart form:
		form, err := ctx.MultipartForm()
		if err != nil {
			return err
		}
		// => *multipart.Form

		// Get all files from "documents" key:
		for key,files:=range form.File{
			fmt.Println(key, files)
			// Loop through files:
			for _, file := range files {
				fmt.Println(file.Filename, file.Size, file.Header["Content-Type"][0])
				// => "tutorial.pdf" 360641 "application/pdf"

				// Save the files to disk:
				err := ctx.SaveFile(file, fmt.Sprintf("./uploads/%s", file.Filename))

				// Check for errors
				if err != nil {
					return err
				}
			}
		}



		return nil
	})

	// start server
	err := server.Start()
	if nil != err {
		t.Error(err)
	}

	// Wait forever.
	server.Join()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func config() *lygo_http_server_config.HttpServerConfig {
	textCfg, _ := lygo_io.ReadTextFromFile("./lygo_http_server_config.json")
	config := new(lygo_http_server_config.HttpServerConfig)
	_ = config.Parse(textCfg)

	return config
}

func onError(errCtx *lygo_http_server_types.HttpServerError) {
	fmt.Println(errCtx.Message, errCtx.Error.Error())
}

func onSocket(ws *lygo_http_server.HttpWebsocketConn) {
	fmt.Println("SOCKET CLIENT", ws.UUID)
	fmt.Println("COUNT CLIENTS", ws.ClientsCount())

	ws.OnDisconnect(func(payload *lygo_http_server.HttpWebsocketEventPayload) {
		fmt.Println("DISCONNECTED", ws.UUID, payload.Error)
		fmt.Println("COUNT CLIENTS", ws.ClientsCount())
	})
	ws.OnMessage(func(payload *lygo_http_server.HttpWebsocketEventPayload) {
		fmt.Println("MESSAGE", ws.UUID, string(payload.Message.Data))
	})

	// send message to myself
	message := map[string]interface{}{
		"message": "HELLO",
		"sender":  "Sample test",
	}
	data, err := json.Marshal(message)
	if nil != err {
		panic(err)
	}
	ws.SendData(data)
}
