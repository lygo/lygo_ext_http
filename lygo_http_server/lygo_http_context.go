package lygo_http_server

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"github.com/gofiber/fiber/v2"
	"net/url"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type HttpContext struct {

	//-- private --//
	ctx *fiber.Ctx
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func WrapContext(ctx *fiber.Ctx) *HttpContext {
	instance := new(HttpContext)
	instance.ctx = ctx

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *HttpContext) GetQueryParams() map[string][]string {
	if nil != instance && nil != instance.ctx {
		path := instance.ctx.OriginalURL()
		if len(path) > 0 {
			uri, err := url.Parse(path)
			if nil == err {
				query := uri.Query()
				if nil != query && len(query) > 0 {
					return query
				}
			}
		}
	}
	return nil
}

func (instance *HttpContext) GetParams() map[string]interface{} {
	response := make(map[string]interface{})
	if nil != instance && nil != instance.ctx {
		// query
		query := instance.GetQueryParams()
		if nil != query && len(query) > 0 {
			for k, v := range query {
				if len(v) == 1 {
					response[k] = v[0]
				} else {
					response[k] = v
				}
			}
		}

		// body
		body := instance.GetBodyAsMap()
		if len(body) > 0 {
			for k, v := range body {
				response[k] = v
			}
		}
	}
	return response
}

func (instance *HttpContext) GetBody() []byte {
	if nil != instance && nil != instance.ctx {
		return instance.ctx.Body()
	}
	return []byte{}
}

func (instance *HttpContext) GetBodyAsString() string {
	response:= string(instance.GetBody())
	return response
}

func (instance *HttpContext) GetBodyAsMap() map[string]interface{} {
	var response map[string]interface{}
	if nil != instance && nil != instance.ctx {
		text := instance.GetBodyAsString()
		if lygo_regex.IsValidJsonObject(text) {
			err := lygo_json.Read(text, &response)
			if nil != err {
				return response
			}
		} else if strings.Index(text, "&") > 0 {
			uri, err := url.Parse("?" + text)
			if nil == err {
				query := uri.Query()
				if nil != query && len(query) > 0 {
					response = make(map[string]interface{})
					for k, v := range query {
						if len(v) == 1 {
							value := v[0]
							if lygo_regex.IsValidJsonObject(value) {
								item := map[string]interface{}{}
								err := lygo_json.Read(value, &item)
								if nil == err {
									response[k] = item
								}
							} else {
								response[k] = value
							}
						}
					}
				}
			}
		}
	}
	return response
}
