package httpserver

import (
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"sync"
	"time"
)

const delay = 1
const EventOnFileChanged = "on_file_changed"

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type ServerMonitor struct {
	files         []string // files to monitor for change
	filesChecksum []string

	events  *lygo_events.Emitter
	fileMux sync.Mutex
	ticker  *lygo_events.EventTicker
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewMonitor(files []string) *ServerMonitor {
	instance := new(ServerMonitor)
	instance.files = files
	instance.events = lygo_events.NewEmitter()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ServerMonitor) Start() {
	if nil != instance && nil == instance.ticker {
		instance.init()
	}
}

func (instance *ServerMonitor) Stop() {
	if nil != instance && nil != instance.ticker {
		instance.ticker.Stop()
		instance.ticker = nil
	}
}

func (instance *ServerMonitor) OnFileChanged(callback func(event *lygo_events.Event)) {
	if nil != instance && nil != instance.events {
		instance.events.On(EventOnFileChanged, callback)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ServerMonitor) init() {
	instance.filesChecksum = make([]string, len(instance.files))
	for idx, file := range instance.files {
		if b, _ := lygo_paths.Exists(file); b {
			text, err := lygo_io.ReadTextFromFile(file)
			if nil != err {
				instance.filesChecksum[idx] = ""
			} else {
				instance.filesChecksum[idx] = lygo_crypto.MD5(text)
			}
		} else {
			instance.filesChecksum[idx] = ""
		}
	}
	instance.ticker = lygo_events.NewEventTicker(delay*time.Second, func(t *lygo_events.EventTicker) {
		instance.check()
	})
	instance.ticker.Start()
}

func (instance *ServerMonitor) check() {
	if nil != instance {
		instance.fileMux.Lock()
		defer instance.fileMux.Unlock()

		for idx, file := range instance.files {
			if b, _ := lygo_paths.Exists(file); b {
				text, err := lygo_io.ReadTextFromFile(file)
				if nil == err {
					key := lygo_crypto.MD5(text)
					if key != instance.filesChecksum[idx] {
						instance.filesChecksum[idx] = key
						instance.events.EmitAsync(EventOnFileChanged, file)
						break
					}
				}
			}
		}
	}
}
