# LyGo Http Client & Server #

![](icon.png)

Client and Server HTTP implementation based on FastHTTP library and Fiber.


## Server Dependencies ##

```
go get -u bitbucket.org/lygo/lygo_events
go get -u bitbucket.org/lygo/lygo_commons
```

```
go get -u github.com/gofiber/fiber/v2
go get -u github.com/gofiber/fiber/v2/middleware/recover
go get -u github.com/gofiber/fiber/v2/middleware/cors
go get -u github.com/gofiber/fiber/v2/middleware/compress
go get -u github.com/gofiber/fiber/v2/middleware/limiter
go get -u github.com/gofiber/fiber/v2/middleware/requestid

go get -u github.com/gofiber/websocket/v2
```

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_http@v0.1.21
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.21
git push origin v0.1.21
```