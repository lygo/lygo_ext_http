package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_ext_http/httpserver"
	"github.com/gofiber/fiber/v2"
	"testing"
	"time"
)

func TestServer(t *testing.T) {

	server := httpserver.NewHttpServer("./server", nil, nil)
	errs := server.Configure(80, 443, "./cert/ssl.cert", "./cert/ssl.key", "./www", false).
		All("/api/*", handleAPI).
		Start()
	if len(errs)>0{
		t.Error(errs)
		t.FailNow()
	}
	_ = lygo_exec.Open("http://localhost")

	errs = server.Restart()
	if len(errs)>0{
		t.Error(errs)
		t.FailNow()
	}

	time.Sleep(20*time.Minute)
}

func handleAPI(ctx *fiber.Ctx) error {
	_, _ = ctx.WriteString("API RESPONSE")
	return nil
}
