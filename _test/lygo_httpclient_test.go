package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_http/httpclient"
	"fmt"
	"github.com/valyala/fasthttp"
	"log"
	"net/url"
	"testing"
)

func TestSimple(t *testing.T) {

	client := new(httpclient.HttpClient)

	// Fetch google page via local proxy.
	fmt.Println("https://gianangelogeminiani.me")
	resp, err := client.Get("https://gianangelogeminiani.me")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if resp.StatusCode != fasthttp.StatusOK {
		log.Fatalf("Unexpected status code: %d. Expecting %d", resp.StatusCode, fasthttp.StatusOK)
	}
	useResponseBody(resp.Body)

	// Fetch foobar page via local proxy. Reuse body buffer.
	fmt.Println("https://botika.ai/")
	resp, err = client.Get("https://botika.ai/")
	if resp.StatusCode != fasthttp.StatusOK {
		log.Fatalf("Unexpected status code: %d. Expecting %d", resp.StatusCode, fasthttp.StatusOK)
	}
	if err != nil {
		log.Fatalf("Error when loading google page through local proxy: %s", err)
	}
	useResponseBody(resp.Body)

}

func TestTinyURL(t *testing.T) {
	urlFull := "http://localhost:63343/ritiro_io_client/index.html?_ijt=qbk2r2ocijg43343og9ivnvr4o#!/02_viewer/menu/ee63b7f4-1766-487e-8762-3a2710320158/04eff121-d533-edc9-7fc2-ebc393895250"
	client := new(httpclient.HttpClient)
	callUrl := "http://tinyurl.com/api-create.php?url=" + url.QueryEscape(urlFull)
	response, err := client.Get(callUrl)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(string(response.Body))
}

func TestDownload(t *testing.T) {
	client := new(httpclient.HttpClient)

	file := "https://gianangelogeminiani.me/download/architecture.png"
	fmt.Println(file)
	resp, err := client.Get(file)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if resp.StatusCode != fasthttp.StatusOK {
		log.Fatalf("Unexpected status code: %d. Expecting %d", resp.StatusCode, fasthttp.StatusOK)
	}

	fileName := "./downloads/architecture.png"
	_ = lygo_paths.Mkdir(fileName)
	_, err = lygo_io.WriteBytesToFile(resp.Body, fileName)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println(fileName)
}

func TestPost(t *testing.T) {
	endpoint := "https://httpbin.org/post"
	body:=map[string]interface{}{
		"param1":"hello",
	}
	client := httpclient.NewHttpClient()
	client.AddHeader("Content-Type", "application/json")
	response, err := client.Post(endpoint, body)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(string(response.Body))
}

func TestSkebbySMSGateway(t *testing.T) {
	endpoint := "https://api.skebby.it/API/v1.0/REST/sms"
	recipient := []string{"347....."}
	body:=map[string]interface{}{}
	body["returnCredits"] = true
	body["returnCredits"] = true
	body["message"] = "Hello"
	body["message_type"] = "SI"
	body["recipient"] = recipient

	client := httpclient.NewHttpClient()
	client.AddHeader("Content-Type", "application/json")
	client.AddHeader("user_key", "1234")
	client.AddHeader("Session_key", "aDEAQER")
	response, err := client.Post(endpoint, body)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(string(response.Body))
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func useResponseBody(body []byte) {
	fmt.Println(string(body))
}
