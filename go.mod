module bitbucket.org/lygo/lygo_ext_http

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_events v0.1.10
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/websocket/v2 v2.0.10 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/klauspost/compress v1.13.5 // indirect
	github.com/valyala/fasthttp v1.30.0
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210909193231-528a39cd75f3 // indirect
)
